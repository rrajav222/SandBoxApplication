﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace SandBoxApplication
{
    class SandboxCMD
    {
        public static void CommandLineInterface(string[] args)
        {
            //three arguments are  required for the command line tool to work
            //argument 1 is filepath, argument 2 is application parameters, argument 3 is application permissions
            if (args.Count() == 3)
            {
                try
                {
                    string applicationPath = args[0].ToString();
                    string applicationParameters = args[1].ToString();
                    string[] permissionList = ManagePermissions(args[2]);
                    //Console.Clear();
                    Sandbox.ExecutingMethod(applicationPath, applicationParameters, permissionList);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message}");
                }
            }
            else
            {
                //send a help message contaiining the permissions list and the correct format for entering the necessary arguments in console mode
                if (args.Contains("-h") || args.Contains("-H"))
                {
                    Console.WriteLine("Format:-\nFilepath_of_SandboxApplication.exe \"<application path>\" \"<application parameters>(leave empty if no paramenters)\" \"<permissions>\"");
                    Console.WriteLine("Note that the minimun permission required to run an application is the -exe (Execution) permission");
                    Console.WriteLine("-un = Unrestricted permissions");
                    Console.WriteLine("-dp = Data Protection permissions");
                    Console.WriteLine("-env = Environment permissions");
                    Console.WriteLine("-exe = Execution permissions");
                    Console.WriteLine("-fd = File Dialog permissions");
                    Console.WriteLine("-io = IO permissions");
                    Console.WriteLine("-is = Isolated Storage permissions");
                    Console.WriteLine("-kc = Key Container permissions");
                    Console.WriteLine("-med = Media permissions");
                    Console.WriteLine("-pr = Principal permissions");
                    Console.WriteLine("-ref = Reflection permissions");
                    Console.WriteLine("-reg = Registry permissions");
                    Console.WriteLine("-sec = Security permissions");
                    Console.WriteLine("-sni = StrongName identity permissions");
                    Console.WriteLine("-st = Store permissions");
                    Console.WriteLine("-ui = UI permissions");
                    Console.WriteLine("-we = Web permissions");
                    Console.WriteLine("-web = Web Browser permissions");
                    Console.WriteLine("-zi = Zone Identity permissions");
                }
                else
                {
                    Console.WriteLine("Use -h for Help and Permissions List");
                }
            }
        }
        private static string[] ManagePermissions(string args)
        {
            //checks for the argument that is passed as permission and adds the respective permissions in an array of strings
            List<string> permList = new List<string>();
            if (args.Contains("-un")) permList.Add("-un");
            if (args.Contains("-dp")) permList.Add("-dp");
            if (args.Contains("-env")) permList.Add("-env");
            if (args.Contains("-exe")) permList.Add("-exe");
            if (args.Contains("-fd")) permList.Add("-fd");
            if (args.Contains("-io")) permList.Add("-io");
            if (args.Contains("-is")) permList.Add("-is");
            if (args.Contains("-kc")) permList.Add("-kc");
            if (args.Contains("-med")) permList.Add("-med");
            if (args.Contains("-pr")) permList.Add("-pr");
            if (args.Contains("-ref")) permList.Add("-ref");
            if (args.Contains("-reg")) permList.Add("-reg");
            if (args.Contains("-sec")) permList.Add("-sec");
            if (args.Contains("-st")) permList.Add("-st");
            if (args.Contains("-sni")) permList.Add("-sni");
            if (args.Contains("-ui")) permList.Add("-ui");
            if (args.Contains("-web")) permList.Add("-web");
            if (args.Contains("-we")) permList.Add("-we");
            if (args.Contains("-zi")) permList.Add("-zi");
            string[] permissionSet = permList.ToArray();
            return permissionSet;
        }

    }
}
