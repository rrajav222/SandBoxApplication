﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security;
using System.Security.Policy;
using System.Security.Permissions;
using System.Reflection;
using System.Runtime.Remoting;
using System.Net;

[assembly: AssemblyKeyFile("sandboxkey.snk")]
namespace SandBoxApplication
{
    //Most of the code for the Sanbox class is directly obtained from the official Microsoft documentation
    //on running partially trusted code inside a Sandbox
    //link: https://docs.microsoft.com/en-us/previous-versions/dotnet/framework/code-access-security/how-to-run-partially-trusted-code-in-a-sandbox?redirectedfrom=MSDN
    class Sandbox : MarshalByRefObject
    {
        public static void ExecutingMethod(string pathToUntrusted, string parametersList, string[] permissionList)
        {
            
            string appFilePath = Path.GetDirectoryName(pathToUntrusted);
            string applicationAssemblyName = Path.GetFileNameWithoutExtension(pathToUntrusted);

            PermissionSet permSet = GetPermissionSet(permissionList);
           
            //We may want to prevent the user from running the our sandbox application from within the same sandbox application
            //hence we check the two assemblies and if they match, we prevent the sandbox from running
            //if (Assembly.LoadFrom(pathToUntrusted).FullName == Assembly.GetExecutingAssembly().ToString())
            //{
            //    Console.WriteLine("Cannot run sandbox from within the sandbox");
            //    return;
            //}
           
            AppDomainSetup adSetup = new AppDomainSetup();
            adSetup.ApplicationBase = Path.GetFullPath(appFilePath);

            //We want the sandboxer assembly's strong name, so that we can add it to the full trust list.
            StrongName fullTrustAssembly = typeof(Sandbox).Assembly.Evidence.GetHostEvidence<StrongName>();

            //Create the AppDomain 
            AppDomain newDomain = AppDomain.CreateDomain("Sandbox", null, adSetup, permSet, fullTrustAssembly);

            string[] parameters = parametersList.Split(' ');
            //foreach(string p in parameters)
            //{
            //    Console.WriteLine(p);
            //}
            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] ----- \"{applicationAssemblyName}\" Started -----");
            newDomain.ExecuteAssembly(pathToUntrusted, parameters);
            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] ----- \"{applicationAssemblyName}\" Ended -----");
        }

        static private PermissionSet GetPermissionSet(string[] permissionList)
        {
            //Create a PermissionSet and Add permissions to it based on the array contents
            //List of permissions based on Microsoft documentation that defines classes that control access to operations and resources
            //link: https://docs.microsoft.com/en-us/dotnet/api/system.security.permissions?view=dotnet-plat-ext-6.0

            PermissionSet permSet = new PermissionSet(PermissionState.None);
            if (permissionList.Contains("-un")) permSet = new PermissionSet(PermissionState.Unrestricted);
            if (permissionList.Contains("-dp")) permSet.AddPermission(new DataProtectionPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-env")) permSet.AddPermission(new EnvironmentPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-exe")) permSet.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));
            if (permissionList.Contains("-fd")) permSet.AddPermission(new FileDialogPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-io"))  permSet.AddPermission(new FileIOPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-is")) permSet.AddPermission(new IsolatedStorageFilePermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-kc")) permSet.AddPermission(new KeyContainerPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-med")) permSet.AddPermission(new MediaPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-pr")) permSet.AddPermission(new PrincipalPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-ref")) permSet.AddPermission(new ReflectionPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-reg")) permSet.AddPermission(new RegistryPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-sec")) permSet.AddPermission(new SecurityPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-st")) permSet.AddPermission(new StorePermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-sni")) permSet.AddPermission(new StrongNameIdentityPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-ui")) permSet.AddPermission(new UIPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-web")) permSet.AddPermission(new WebBrowserPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-we")) permSet.AddPermission(new WebPermission(PermissionState.Unrestricted));
            if (permissionList.Contains("-zi")) permSet.AddPermission(new ZoneIdentityPermission(PermissionState.Unrestricted));
            return permSet;
        }

    }
}
