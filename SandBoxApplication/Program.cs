﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace SandBoxApplication
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
           //check for arguments
           //if arguments are passed, SandboxCMD runs the application
           //else, open SandboxUI to run the application in interactive UI
           if (args.Length > 0 && args[0] != string.Empty)
            {
                SandboxCMD.CommandLineInterface(args);
                Environment.Exit(0);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new SandBoxUI());
            }
        }
    }
}
