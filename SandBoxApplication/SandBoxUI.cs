﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Reflection;

namespace SandBoxApplication
{
    public partial class SandBoxUI : Form
    {
        //We may want to hide the console window when SandboxUI is loaded because we don't need it at the moment
        //The code was obtained from stackoverflow
        //link: https://stackoverflow.com/questions/3571627/show-hide-the-console-window-of-a-c-sharp-console-application

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        private void HideConsole()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
        }

        private void ShowConsole()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_SHOW);
        }


        public SandBoxUI()
        {
            InitializeComponent();
            HideConsole();
        }

        public void LogThis(string message)
        {
            // Logging to the Output Console
            outputConsole.AppendText($"[{DateTime.Now.ToString("HH:mm:ss")}] {message} \n");
        }

        private void SandBoxUI_Load(object sender, EventArgs e)
        {
        }


        private void openFileDialogButton_Click(object sender, EventArgs e)
        {
            //select assembly to execute and set its path to the textbox
            DialogResult appDialogResult = openFileDialog.ShowDialog();
            if (appDialogResult == DialogResult.OK)
            {
                filePathTextBox.Text = openFileDialog.FileName;
                LogThis(Path.GetFileName(filePathTextBox.Text) + " selected.");
            }
        }

        private string[] ManagePermissions()
        {
            //searches for the selected checkboxes and adds the permission names to an array of strings
            List<string> permList = new List<string>(); ;
            if (unrestrictedCheckbox.Checked) permList.Add("-un");
            if (fileIOCheckbox.Checked) permList.Add("-io");
            if (fileUICheckbox.Checked) permList.Add("-ui");
            if (executionCheckbox.Checked) permList.Add("-exe");
            if (dataProtectionCheckbox.Checked) permList.Add("-dp");
            if (environmentCheckbox.Checked) permList.Add("-env");
            if (fileDialogCheckbox.Checked) permList.Add("-fd");
            if (isolatedStoragePermission.Checked) permList.Add("-is");
            if (keyContainerCheckbox.Checked) permList.Add("-kc");
            if (mediaCheckbox.Checked) permList.Add("-med");
            if (principalCheckbox.Checked) permList.Add("-pr");
            if (reflectionCheckbox.Checked) permList.Add("-ref");
            if (registryCheckbox.Checked) permList.Add("-reg");
            if (securityCheckbox.Checked) permList.Add("-sec");
            if (storeCheckbox.Checked) permList.Add("-st");
            if (strongNameCheckbox.Checked) permList.Add("-sni");
            if (webBrowserCheckbox.Checked) permList.Add("-web");
            if (webCheckbox.Checked) permList.Add("-we");
            if (zoneIdentityCheckbox.Checked) permList.Add("-zi");
            string[] permissionSet = permList.ToArray();
            return permissionSet;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }


        private void runButton_Click(object sender, EventArgs e)
        {
            //gets the necessary parameters from the SandboxUI and sends them as parameters to the Sandbox.ExecutingMethod method
            string applicationPath = filePathTextBox.Text;
            string applicationParameters = parametersTextBox.Text;
            string[] permissionList = ManagePermissions();
            string applicationAssemblyName = Path.GetFileNameWithoutExtension(applicationPath);

            try
            {
                //now we show the console to show the assembly that is runnning
                ShowConsole();
                Console.Clear();
                LogThis($"\"{applicationAssemblyName}\" Started -----");
                Sandbox.ExecutingMethod(applicationPath, applicationParameters, permissionList);
                LogThis($"\"{applicationAssemblyName}\" Ended Successfully-----");
            }
            catch (Exception ex)
            {
                //in the event of an exception we hide the console again and show the error message in the log
                HideConsole();
                LogThis($"Exception caught:\n{ex.Message}");
                LogThis($"\"{applicationAssemblyName}\" Ended Unsuccessfully-----");
            }
            
        }

        //method to clear the log
        private void clearLog_Click(object sender, EventArgs e)
        {
            outputConsole.ResetText();
        }

        private void exitConsole_Click(object sender, EventArgs e)
        {
            Console.Clear();
            HideConsole();
        }
    }
}
