﻿
namespace SandBoxApplication
{
    partial class SandBoxUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialogButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.parametersTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.filePathTextBox = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.executionCheckbox = new System.Windows.Forms.CheckBox();
            this.zoneIdentityCheckbox = new System.Windows.Forms.CheckBox();
            this.webBrowserCheckbox = new System.Windows.Forms.CheckBox();
            this.strongNameCheckbox = new System.Windows.Forms.CheckBox();
            this.storeCheckbox = new System.Windows.Forms.CheckBox();
            this.securityCheckbox = new System.Windows.Forms.CheckBox();
            this.registryCheckbox = new System.Windows.Forms.CheckBox();
            this.reflectionCheckbox = new System.Windows.Forms.CheckBox();
            this.principalCheckbox = new System.Windows.Forms.CheckBox();
            this.mediaCheckbox = new System.Windows.Forms.CheckBox();
            this.keyContainerCheckbox = new System.Windows.Forms.CheckBox();
            this.isolatedStoragePermission = new System.Windows.Forms.CheckBox();
            this.fileDialogCheckbox = new System.Windows.Forms.CheckBox();
            this.environmentCheckbox = new System.Windows.Forms.CheckBox();
            this.dataProtectionCheckbox = new System.Windows.Forms.CheckBox();
            this.webCheckbox = new System.Windows.Forms.CheckBox();
            this.fileUICheckbox = new System.Windows.Forms.CheckBox();
            this.fileIOCheckbox = new System.Windows.Forms.CheckBox();
            this.unrestrictedCheckbox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.outputConsole = new System.Windows.Forms.RichTextBox();
            this.runButton = new System.Windows.Forms.Button();
            this.clearLog = new System.Windows.Forms.Button();
            this.exitConsole = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialogButton
            // 
            this.openFileDialogButton.Location = new System.Drawing.Point(693, 26);
            this.openFileDialogButton.Name = "openFileDialogButton";
            this.openFileDialogButton.Size = new System.Drawing.Size(75, 23);
            this.openFileDialogButton.TabIndex = 0;
            this.openFileDialogButton.Text = "Browse";
            this.openFileDialogButton.UseVisualStyleBackColor = true;
            this.openFileDialogButton.Click += new System.EventHandler(this.openFileDialogButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.parametersTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.filePathTextBox);
            this.groupBox1.Controls.Add(this.openFileDialogButton);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(775, 100);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Application Details";
            // 
            // parametersTextBox
            // 
            this.parametersTextBox.Location = new System.Drawing.Point(91, 64);
            this.parametersTextBox.Name = "parametersTextBox";
            this.parametersTextBox.Size = new System.Drawing.Size(597, 20);
            this.parametersTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Parameter(s) :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "File Path :";
            // 
            // filePathTextBox
            // 
            this.filePathTextBox.Location = new System.Drawing.Point(91, 28);
            this.filePathTextBox.Name = "filePathTextBox";
            this.filePathTextBox.Size = new System.Drawing.Size(597, 20);
            this.filePathTextBox.TabIndex = 1;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.executionCheckbox);
            this.groupBox2.Controls.Add(this.zoneIdentityCheckbox);
            this.groupBox2.Controls.Add(this.webBrowserCheckbox);
            this.groupBox2.Controls.Add(this.strongNameCheckbox);
            this.groupBox2.Controls.Add(this.storeCheckbox);
            this.groupBox2.Controls.Add(this.securityCheckbox);
            this.groupBox2.Controls.Add(this.registryCheckbox);
            this.groupBox2.Controls.Add(this.reflectionCheckbox);
            this.groupBox2.Controls.Add(this.principalCheckbox);
            this.groupBox2.Controls.Add(this.mediaCheckbox);
            this.groupBox2.Controls.Add(this.keyContainerCheckbox);
            this.groupBox2.Controls.Add(this.isolatedStoragePermission);
            this.groupBox2.Controls.Add(this.fileDialogCheckbox);
            this.groupBox2.Controls.Add(this.environmentCheckbox);
            this.groupBox2.Controls.Add(this.dataProtectionCheckbox);
            this.groupBox2.Controls.Add(this.webCheckbox);
            this.groupBox2.Controls.Add(this.fileUICheckbox);
            this.groupBox2.Controls.Add(this.fileIOCheckbox);
            this.groupBox2.Controls.Add(this.unrestrictedCheckbox);
            this.groupBox2.Location = new System.Drawing.Point(13, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(775, 132);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Permission Settings";
            // 
            // executionCheckbox
            // 
            this.executionCheckbox.AutoSize = true;
            this.executionCheckbox.Location = new System.Drawing.Point(9, 98);
            this.executionCheckbox.Name = "executionCheckbox";
            this.executionCheckbox.Size = new System.Drawing.Size(73, 17);
            this.executionCheckbox.TabIndex = 18;
            this.executionCheckbox.Text = "Execution";
            this.executionCheckbox.UseVisualStyleBackColor = true;
            // 
            // zoneIdentityCheckbox
            // 
            this.zoneIdentityCheckbox.AutoSize = true;
            this.zoneIdentityCheckbox.Location = new System.Drawing.Point(624, 75);
            this.zoneIdentityCheckbox.Name = "zoneIdentityCheckbox";
            this.zoneIdentityCheckbox.Size = new System.Drawing.Size(88, 17);
            this.zoneIdentityCheckbox.TabIndex = 17;
            this.zoneIdentityCheckbox.Text = "Zone Identity";
            this.zoneIdentityCheckbox.UseVisualStyleBackColor = true;
            // 
            // webBrowserCheckbox
            // 
            this.webBrowserCheckbox.AutoSize = true;
            this.webBrowserCheckbox.Location = new System.Drawing.Point(624, 29);
            this.webBrowserCheckbox.Name = "webBrowserCheckbox";
            this.webBrowserCheckbox.Size = new System.Drawing.Size(90, 17);
            this.webBrowserCheckbox.TabIndex = 16;
            this.webBrowserCheckbox.Text = "Web Browser";
            this.webBrowserCheckbox.UseVisualStyleBackColor = true;
            // 
            // strongNameCheckbox
            // 
            this.strongNameCheckbox.AutoSize = true;
            this.strongNameCheckbox.Location = new System.Drawing.Point(464, 98);
            this.strongNameCheckbox.Name = "strongNameCheckbox";
            this.strongNameCheckbox.Size = new System.Drawing.Size(122, 17);
            this.strongNameCheckbox.TabIndex = 15;
            this.strongNameCheckbox.Text = "StrongName Identity";
            this.strongNameCheckbox.UseVisualStyleBackColor = true;
            // 
            // storeCheckbox
            // 
            this.storeCheckbox.AutoSize = true;
            this.storeCheckbox.Location = new System.Drawing.Point(464, 75);
            this.storeCheckbox.Name = "storeCheckbox";
            this.storeCheckbox.Size = new System.Drawing.Size(51, 17);
            this.storeCheckbox.TabIndex = 14;
            this.storeCheckbox.Text = "Store";
            this.storeCheckbox.UseVisualStyleBackColor = true;
            // 
            // securityCheckbox
            // 
            this.securityCheckbox.AutoSize = true;
            this.securityCheckbox.Location = new System.Drawing.Point(464, 52);
            this.securityCheckbox.Name = "securityCheckbox";
            this.securityCheckbox.Size = new System.Drawing.Size(64, 17);
            this.securityCheckbox.TabIndex = 13;
            this.securityCheckbox.Text = "Security";
            this.securityCheckbox.UseVisualStyleBackColor = true;
            // 
            // registryCheckbox
            // 
            this.registryCheckbox.AutoSize = true;
            this.registryCheckbox.Location = new System.Drawing.Point(464, 29);
            this.registryCheckbox.Name = "registryCheckbox";
            this.registryCheckbox.Size = new System.Drawing.Size(64, 17);
            this.registryCheckbox.TabIndex = 12;
            this.registryCheckbox.Text = "Registry";
            this.registryCheckbox.UseVisualStyleBackColor = true;
            // 
            // reflectionCheckbox
            // 
            this.reflectionCheckbox.AutoSize = true;
            this.reflectionCheckbox.Location = new System.Drawing.Point(318, 98);
            this.reflectionCheckbox.Name = "reflectionCheckbox";
            this.reflectionCheckbox.Size = new System.Drawing.Size(74, 17);
            this.reflectionCheckbox.TabIndex = 11;
            this.reflectionCheckbox.Text = "Reflection";
            this.reflectionCheckbox.UseVisualStyleBackColor = true;
            // 
            // principalCheckbox
            // 
            this.principalCheckbox.AutoSize = true;
            this.principalCheckbox.Location = new System.Drawing.Point(318, 75);
            this.principalCheckbox.Name = "principalCheckbox";
            this.principalCheckbox.Size = new System.Drawing.Size(66, 17);
            this.principalCheckbox.TabIndex = 10;
            this.principalCheckbox.Text = "Principal";
            this.principalCheckbox.UseVisualStyleBackColor = true;
            // 
            // mediaCheckbox
            // 
            this.mediaCheckbox.AutoSize = true;
            this.mediaCheckbox.Location = new System.Drawing.Point(318, 52);
            this.mediaCheckbox.Name = "mediaCheckbox";
            this.mediaCheckbox.Size = new System.Drawing.Size(55, 17);
            this.mediaCheckbox.TabIndex = 9;
            this.mediaCheckbox.Text = "Media";
            this.mediaCheckbox.UseVisualStyleBackColor = true;
            // 
            // keyContainerCheckbox
            // 
            this.keyContainerCheckbox.AutoSize = true;
            this.keyContainerCheckbox.Location = new System.Drawing.Point(318, 29);
            this.keyContainerCheckbox.Name = "keyContainerCheckbox";
            this.keyContainerCheckbox.Size = new System.Drawing.Size(92, 17);
            this.keyContainerCheckbox.TabIndex = 8;
            this.keyContainerCheckbox.Text = "Key Container";
            this.keyContainerCheckbox.UseVisualStyleBackColor = true;
            // 
            // isolatedStoragePermission
            // 
            this.isolatedStoragePermission.AutoSize = true;
            this.isolatedStoragePermission.Location = new System.Drawing.Point(155, 98);
            this.isolatedStoragePermission.Name = "isolatedStoragePermission";
            this.isolatedStoragePermission.Size = new System.Drawing.Size(103, 17);
            this.isolatedStoragePermission.TabIndex = 7;
            this.isolatedStoragePermission.Text = "Isolated Storage";
            this.isolatedStoragePermission.UseVisualStyleBackColor = true;
            // 
            // fileDialogCheckbox
            // 
            this.fileDialogCheckbox.AutoSize = true;
            this.fileDialogCheckbox.Location = new System.Drawing.Point(155, 75);
            this.fileDialogCheckbox.Name = "fileDialogCheckbox";
            this.fileDialogCheckbox.Size = new System.Drawing.Size(75, 17);
            this.fileDialogCheckbox.TabIndex = 6;
            this.fileDialogCheckbox.Text = "File Dialog";
            this.fileDialogCheckbox.UseVisualStyleBackColor = true;
            // 
            // environmentCheckbox
            // 
            this.environmentCheckbox.AutoSize = true;
            this.environmentCheckbox.Location = new System.Drawing.Point(155, 52);
            this.environmentCheckbox.Name = "environmentCheckbox";
            this.environmentCheckbox.Size = new System.Drawing.Size(85, 17);
            this.environmentCheckbox.TabIndex = 5;
            this.environmentCheckbox.Text = "Environment";
            this.environmentCheckbox.UseVisualStyleBackColor = true;
            // 
            // dataProtectionCheckbox
            // 
            this.dataProtectionCheckbox.AutoSize = true;
            this.dataProtectionCheckbox.Location = new System.Drawing.Point(155, 29);
            this.dataProtectionCheckbox.Name = "dataProtectionCheckbox";
            this.dataProtectionCheckbox.Size = new System.Drawing.Size(100, 17);
            this.dataProtectionCheckbox.TabIndex = 4;
            this.dataProtectionCheckbox.Text = "Data Protection";
            this.dataProtectionCheckbox.UseVisualStyleBackColor = true;
            // 
            // webCheckbox
            // 
            this.webCheckbox.AutoSize = true;
            this.webCheckbox.Location = new System.Drawing.Point(624, 52);
            this.webCheckbox.Name = "webCheckbox";
            this.webCheckbox.Size = new System.Drawing.Size(49, 17);
            this.webCheckbox.TabIndex = 3;
            this.webCheckbox.Text = "Web";
            this.webCheckbox.UseVisualStyleBackColor = true;
            // 
            // fileUICheckbox
            // 
            this.fileUICheckbox.AutoSize = true;
            this.fileUICheckbox.Location = new System.Drawing.Point(9, 75);
            this.fileUICheckbox.Name = "fileUICheckbox";
            this.fileUICheckbox.Size = new System.Drawing.Size(93, 17);
            this.fileUICheckbox.TabIndex = 2;
            this.fileUICheckbox.Text = "User Interface";
            this.fileUICheckbox.UseVisualStyleBackColor = true;
            // 
            // fileIOCheckbox
            // 
            this.fileIOCheckbox.AutoSize = true;
            this.fileIOCheckbox.Location = new System.Drawing.Point(9, 52);
            this.fileIOCheckbox.Name = "fileIOCheckbox";
            this.fileIOCheckbox.Size = new System.Drawing.Size(58, 17);
            this.fileIOCheckbox.TabIndex = 1;
            this.fileIOCheckbox.Text = "FileI/O";
            this.fileIOCheckbox.UseVisualStyleBackColor = true;
            // 
            // unrestrictedCheckbox
            // 
            this.unrestrictedCheckbox.AutoSize = true;
            this.unrestrictedCheckbox.Location = new System.Drawing.Point(9, 29);
            this.unrestrictedCheckbox.Name = "unrestrictedCheckbox";
            this.unrestrictedCheckbox.Size = new System.Drawing.Size(83, 17);
            this.unrestrictedCheckbox.TabIndex = 0;
            this.unrestrictedCheckbox.Text = "Unrestricted";
            this.unrestrictedCheckbox.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.outputConsole);
            this.groupBox3.Location = new System.Drawing.Point(13, 299);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(775, 192);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log";
            // 
            // outputConsole
            // 
            this.outputConsole.Location = new System.Drawing.Point(1, 19);
            this.outputConsole.Name = "outputConsole";
            this.outputConsole.ReadOnly = true;
            this.outputConsole.Size = new System.Drawing.Size(774, 167);
            this.outputConsole.TabIndex = 0;
            this.outputConsole.Text = "";
            // 
            // runButton
            // 
            this.runButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runButton.Location = new System.Drawing.Point(331, 262);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(150, 35);
            this.runButton.TabIndex = 4;
            this.runButton.Text = "Run Application";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // clearLog
            // 
            this.clearLog.Location = new System.Drawing.Point(22, 268);
            this.clearLog.Name = "clearLog";
            this.clearLog.Size = new System.Drawing.Size(75, 23);
            this.clearLog.TabIndex = 5;
            this.clearLog.Text = "Clear Log";
            this.clearLog.UseVisualStyleBackColor = true;
            this.clearLog.Click += new System.EventHandler(this.clearLog_Click);
            // 
            // exitConsole
            // 
            this.exitConsole.Location = new System.Drawing.Point(706, 268);
            this.exitConsole.Name = "exitConsole";
            this.exitConsole.Size = new System.Drawing.Size(75, 23);
            this.exitConsole.TabIndex = 6;
            this.exitConsole.Text = "Exit Console";
            this.exitConsole.UseVisualStyleBackColor = true;
            this.exitConsole.Click += new System.EventHandler(this.exitConsole_Click);
            // 
            // SandBoxUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 490);
            this.Controls.Add(this.exitConsole);
            this.Controls.Add(this.clearLog);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "SandBoxUI";
            this.Text = "SandBoxUI";
            this.Load += new System.EventHandler(this.SandBoxUI_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button openFileDialogButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox filePathTextBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox outputConsole;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox parametersTextBox;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.CheckBox webCheckbox;
        private System.Windows.Forms.CheckBox fileUICheckbox;
        private System.Windows.Forms.CheckBox fileIOCheckbox;
        private System.Windows.Forms.CheckBox unrestrictedCheckbox;
        private System.Windows.Forms.Button clearLog;
        private System.Windows.Forms.Button exitConsole;
        private System.Windows.Forms.CheckBox isolatedStoragePermission;
        private System.Windows.Forms.CheckBox fileDialogCheckbox;
        private System.Windows.Forms.CheckBox environmentCheckbox;
        private System.Windows.Forms.CheckBox dataProtectionCheckbox;
        private System.Windows.Forms.CheckBox strongNameCheckbox;
        private System.Windows.Forms.CheckBox storeCheckbox;
        private System.Windows.Forms.CheckBox securityCheckbox;
        private System.Windows.Forms.CheckBox registryCheckbox;
        private System.Windows.Forms.CheckBox reflectionCheckbox;
        private System.Windows.Forms.CheckBox principalCheckbox;
        private System.Windows.Forms.CheckBox mediaCheckbox;
        private System.Windows.Forms.CheckBox keyContainerCheckbox;
        private System.Windows.Forms.CheckBox zoneIdentityCheckbox;
        private System.Windows.Forms.CheckBox webBrowserCheckbox;
        private System.Windows.Forms.CheckBox executionCheckbox;
    }
}